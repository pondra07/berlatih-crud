<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@Home');
Route::get('/form', 'AuthController@register');
Route::post('/welcome', 'AuthController@welcome');

// untuk menambah templete
Route::get('/table', function(){
    return view('laravel.table');
});

Route::get('/data-tables', function(){
    return view('laravel.data-tables');
});

// CRUD cast 
// untuk menambahkan form 
Route::get('/cast/create', 'castController@create');
// untuk memasukan data ke database
Route::post('/cast', 'castController@store');
// menampilkan semua list data cast 
Route::get('/cast', 'castController@index');
// menampilkan detail data 
Route::get('/cast/{cast_id}', 'castController@show');
// menampilkan untuk edit 
Route::get('/cast/{cast_id}/edit', 'castController@edit');
// Updte berdasarkan ID 
Route::put('/cast/{cast_id}', 'castController@update');
// fungsi delete berdasarkan id
Route::delete('/cast/{cast_id}', 'castController@destroy');