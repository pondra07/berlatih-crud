@extends('layout.master')

@section('judul')
    Edit Pemain Film {{$cast->nama}}
@endsection

@section('content')
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Nama Pemain Film</label>
            <input type="text" class="form-control" value="{{$cast->nama}}" name="nama" placeholder="Masukkan Nama Pemain Film">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>umur</label>
            <input type="number" class="form-control" value="{{$cast->umur}}" name="umur" placeholder="Masukan Umur Pemain Film"
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>bio</label>
            <textarea name="bio" id="" class="form-control" cols="30" rows="10">{{$cast->bio}}</textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
@endsection