@extends('layout.master')

@section('judul')
    Tambah pemain film baru
@endsection

@section('content')
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
            <label>Nama Pemain Film</label>
            <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama Pemain Film">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>umur</label>
            <input type="number" class="form-control" name="umur" placeholder="Masukan Umur Pemain Film"
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>bio</label>
            <textarea name="bio" id="" class="form-control" cols="30" rows="10"></textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
@endsection