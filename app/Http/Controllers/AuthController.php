<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('halaman.form');
    }

    public function welcome()
    {
        return "Selamat Datang!
        Terima kasih telah bergabung dengan SanberBook. Social media kita bersama!";
    }
}
